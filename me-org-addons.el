;;; me-org-addons.el --- addon 4 journal day time evaluations  -*- lexical-binding: t; -*-

;; me-org-addons.el holds user oriented elisp functions helping to
;; to calculate tags-ordered duration sums from journal entries of a
;; selected day.
;;
;; Functions are made for journal files with org capturing, which hold
;; headline trees in commonly used structure
;; year (level 1) → month (level 2) → day (level 3) → dey entries (level 4)
;;
;; Usage:
;;  - compile or load this file
;;  - put cursor (point) at a day entry or at a day heading (level 4 or
;;    level 3 headline)
;;  - call function me-org-durationsums-o-day
;;  - (TODO: a simple configurable keybinding)
;;
;; Result
;;  - are shown in a popup message and in *messages* buffer
;;
;; Author: Mathias Erhart <uhnae@mailbox.org>
;;
;; License: GNU General Public License version 3 (or at your option) any
;; later version.

;;; Code:

;;; org APIs, especially org-element and testing facilities with ERT needed!

(require 'cl-lib)
(require 'ert)
(require 'org)
(require 'org-element)

(defgroup me-org-addons nil
  "org addons - esp for time calculations in org files"
  :group 'Emacs)

(defcustom me-org-tags-order-abbrevs
  '((("foo") "Foo") (("bar") "Bar") (("foo" "bar") "FooBar") (() "Pause"))
  "assoc list that maps tag or tag combinations to user strings
tag or combination of tags in their textual representation
the mapped values (strings) will be used as display string for
tag (combinations)"
  :type '(repeat (list (repeat string) string))
  :group 'me-org-addons)

;;; common helper functions

;; predicate for given plist: guess, if list is a plist with symbols as keys

(defun plist-with-symbols-as-keys-p-rec (list)
  "Determine if LIST is a property list (plist) where all keys are symbols.
Surprisingly this predicate returns t when LIST is nil!
Needed helper fun for plist-with-symbols-as-keys-p."
  (let ((key (car list))
        (rest (cddr list)))
    (if (symbolp key)
      (if rest
          (plist-with-symbols-as-keys-p-rec rest)
        t)
      nil)))

(defun plist-with-symbols-as-keys-p (list)
  "Determine if LIST is a property list (plist) where all keys are symbols.
Predicate unsurprisingly results in nil when LIST is nil."
  (if (and list
           (listp list))
      (plist-with-symbols-as-keys-p-rec list)
    nil))

(ert-deftest me-org-test-plist-predicate ()
  "Test plist-with-symbols-as-keys-p with common usages
and misusages"
  ;;; should eval to t
  (should (plist-with-symbols-as-keys-p '(a)))
  (should (plist-with-symbols-as-keys-p '(a 1)))
  (should (plist-with-symbols-as-keys-p '(a 5.4321)))
  (should (plist-with-symbols-as-keys-p '(a 5.4321e02)))
  (should (plist-with-symbols-as-keys-p '(a "string")))
  (should (plist-with-symbols-as-keys-p '(a ?\b)))
  (should (plist-with-symbols-as-keys-p '(a (1 b "string"))))
  (should (plist-with-symbols-as-keys-p '(a 1 b)))
  (should (plist-with-symbols-as-keys-p '(a "string" b 1)))
  (should (plist-with-symbols-as-keys-p '(a ?\b c 1)))
  (should (plist-with-symbols-as-keys-p '(a "string" b 1 c d)))
  (should (plist-with-symbols-as-keys-p '(a 1 b "string" c d e)))
  ;;; should eval to nil
  (should-not (plist-with-symbols-as-keys-p '()))
  (should-not (plist-with-symbols-as-keys-p nil))
  (should-not (plist-with-symbols-as-keys-p 1))
  (should-not (plist-with-symbols-as-keys-p "string"))
  (should-not (plist-with-symbols-as-keys-p ?\a))
  (should-not (plist-with-symbols-as-keys-p 1.2345))
  (should-not (plist-with-symbols-as-keys-p 1.2345e02))
  (should-not (plist-with-symbols-as-keys-p '(1 c)))
  (should-not (plist-with-symbols-as-keys-p '((a b))))
  (should-not (plist-with-symbols-as-keys-p '("string" a)))
  (should-not (plist-with-symbols-as-keys-p '(4.123e02 a)))
  (should-not (plist-with-symbols-as-keys-p '((a) b)))
  (should-not (plist-with-symbols-as-keys-p '((a b) c)))
  (should-not (plist-with-symbols-as-keys-p '(a b 1 c)))
  (should-not (plist-with-symbols-as-keys-p '(a b "string" c)))
  (should-not (plist-with-symbols-as-keys-p '(a b 4.123e02 c)))
  (should-not (plist-with-symbols-as-keys-p '(a b (c) d)))
  (should-not (plist-with-symbols-as-keys-p '(a b (c d) e)))
  (should-not (plist-with-symbols-as-keys-p '(a 1 2 b)))
  (should-not (plist-with-symbols-as-keys-p '((a b) c d e)))
  )

;; predicate which checks if given property value pairs are all
;; part of a property list

(defun plist-contains-p-rec (plist property-value-pairs-as-list)
  "Recursive helper function reading property-value-pairs from
PROPERTY-VALUE-PAIRS-AS-LIST as long as each pair can be found in PLIST.
Recursion returns t when property-value-pairs-as-list is reduced to nil."
  (let* ((property-value-pair (car property-value-pairs-as-list))
         (property (car property-value-pair))
         (value (cadr property-value-pair)))
    ;;; is property-value-pair a list?
    ;;;   if do more else return nil!
    (if (listp property-value-pair)
        ;;; is property-value-pair nil / a empty list?
        ;;;   if return t (recursion successfully finished) else do more
        (if (not property-value-pair)
            t
          (let ((plist-value (plist-get plist property)))
            ;;; ensure that property is part of plist
            ;;;   and value is equal to plist-value
            ;;; if not return nil
            ;;;   else return result of recursive call
            (and (plist-member plist property)
                 (equal plist-value value)
                 (plist-contains-p-rec plist
                                       (cdr property-value-pairs-as-list)))))
      nil)))

(defun plist-contains-p (plist &rest property-value-pairs)
  "Check all PROPERTY-VALUE-PAIRS if they are part of plist
PLIST. Each pair is a two element list consisting of a PROPERTY
as first and a VALUE as second list element.

PLIST should be a valid property list - behaviour in other
cases is *not specified*.

If all pairs of PROPERTY-VALUE-PAIRS can be found in PLIST
return t.

If one of the given PROPERTY-VALUE-PAIRS is not part of
PLIST return nil.

Special case: if PROPERTY-VALUE-PAIRS is nil return nil."
  (if (or (not property-value-pairs)
          (equal property-value-pairs '(nil)))
      nil
    (plist-contains-p-rec plist property-value-pairs)))

(ert-deftest me-org-test-plist-contains-predicate ()
  "Test plist-contains-p with common usages"
  ;;; should eval to t
  (should (plist-contains-p '(a 1 b "string" c nil d e)
                            '(a 1)))
  (should (plist-contains-p '(a 1 b "string" c nil d e)
                            '(b "string")))
  (should (plist-contains-p '(a 1 b "string" c nil d e)
                            '(b "string") '(a 1)))
  (should (plist-contains-p '(a 1 b "string" c nil d e)
                            '(a 1) '(b "string") '(c)))
  (should (plist-contains-p '(a 1 b "string" c nil d e)
                            '(a 1) '(b "string") '(c nil)))
  (should (plist-contains-p '(a 1 b "string" c nil d e)
                            '(c) '(b "string")))
  (should (plist-contains-p '(a 1 b "string" c nil d e)
                            '(c nil) '(b "string")))
  (should (plist-contains-p '(a 1 b "string" c nil d e)
                            '(c) '(a 1)))
  (should (plist-contains-p '(a 1 b "string" c nil d e)
                            '(c nil) '(a 1)))
  (should (plist-contains-p '(a 1 b "string" c nil d (1 "str" e))
                            '(c nil) '(d (1 "str" e))))
  ;;; should eval to nil
  (should-not (plist-contains-p '(a 1 b "string" c nil d e)
                                nil))
  (should-not (plist-contains-p '(a 1 b "string" c nil d e)
                                '(a 1) '(b 2)))
  (should-not (plist-contains-p '(a 1 b "string" c nil d e)
                                '(b 2) '(a 1)))
  (should-not (plist-contains-p '(a 1 b "string" c nil d e)
                                '(b "string") '(c x)))
  (should-not (plist-contains-p '(a 1 b "string" c nil d e)
                                '(b "gnmpf")))
  (should-not (plist-contains-p '(a 1 b "string" c nil d (1 "str" e))
                                '(c nil) '(d (1 "str" xxx))))
  (should-not (plist-contains-p '(a 1 b "string" c nil d e)
                                nil))
  ;;; errors because of not type matching property-value-pairs
  (should-error (plist-contains-p '(a 1 b "string" c nil d e)
                                  '(a 1) 'xyz) :type 'wrong-type-argument)
  (should-error (plist-contains-p '(a 1 b "string" c nil d e)
                                  'xyz) :type 'wrong-type-argument)
  (should-error (plist-contains-p '(a 1 b "string" c nil d e)
                                  'xyz '(a 1)) :type 'wrong-type-argument)
  (should-error (plist-contains-p '(a 1 b "string" c nil d e)
                                "otherstring") :type 'wrong-type-argument)
  (should-error (plist-contains-p '(a 1 b "string" c nil d e)
                                1) :type 'wrong-type-argument)
  )

;; shortenstring and prettystring for width limited prettified output

(defun shortenstring (str max-length)
  "Shorten STR to MAX-LENGTH if longer.

If length of str exceeds max-length shorten it to max-length minus 3,
instead an ellipsis with 3 points is appended.

Examples:
(shortenstring \"Hallo\" 7) -> \"Hallo\" (unaltered)
(shortenstring \"That is too long\"   10) -> \"That is...\""
  (if (> (length str) max-length)
      (format "%s..." (substring str 0 (- max-length 3)))
    str))

(defun prettystring (str max-length)
  "Replaces line breaks in STR by a unicode symbol and then
shorten string"
  (shortenstring (string-replace "\n" "↵" str) max-length))

(ert-deftest me-org-test-prettyfying ()
  "Test prettystring with common usages"

  (should (equal "abcdefghi" (prettystring "abcdefghi" 20)))
  (should (equal "..." (prettystring "abcdefghi" 3)))
  (should (equal "a..." (prettystring "abcdefghi" 4)))
  (should (equal "abcdefghi" (prettystring "abcdefghi" 9)))
  (should (equal "abcdef..." (prettystring "abcdefghij" 9)))
  (should (equal "abcdefg..." (prettystring "abcdefghijk" 10))))

;; remove text properties of an given string

(defun me-remove-text-properties (text)
  "Return TEXT without any text properties"
  ;; function is derived from a hint at
  ;; https://emacs.stackexchange.com/questions/31225/how-to-strip-decorations-text-properties-from-a-string
  (let* ((proc-text (format "%s" text))
         (len (length proc-text)))
    (set-text-properties 0 len nil proc-text)
    proc-text))

;; TODO (?) some tests? 

;; hh-mm-diff - time difference between start time and an end time
;; (only at same day, in the right order start time before end time)
;; start and end time can only be given in hours and minutes!

(define-error 'me-times-order-error
  "START time is LATER than END time!")

(defun hh-mm-diff (start end)
  "Calculate difference between START and END time.
START and END are lists each with hours and minutes

Returns difference time as list (hours minutes)."
  (let ((h-start (car start))
        (m-start (cadr start))
        (h-end (car end))
        (m-end (cadr end)))
    (or (> h-end h-start)
        (and (= h-end h-start)
	     (>= m-end m-start))
        (signal
         'me-times-order-error
         (format "hh-mm-diff tried to calc %d:%d minus %d:%d!"
                 h-end m-end h-start m-start)))
    (let ((h-diff (- h-end h-start))
	  (m-diff (- m-end m-start)))
      (if (< m-diff 0)
	  (list (- h-diff 1) (+ 60 m-diff))
        (list h-diff m-diff)))))

(ert-deftest me-org-test-hh-mm-diff ()
  "Test hh-mm-diff with common cases"
  (should (equal '(0 0) (hh-mm-diff
			 '(17 21) '(17 21))))
  (should (equal '(3 22) (hh-mm-diff
			  '(17 49) '(21 11))))
  (should (equal '(3 22) (hh-mm-diff
			  '(11 22) '(14 44))))
  (should (equal '(0 22) (hh-mm-diff
			  '(11 22) '(11 44))))
  (should (equal '(0 2) (hh-mm-diff
			 '(1 59) '(2 1))))
  (should (equal '(1 2) (hh-mm-diff
			 '(1 59) '(3 1))))
  (should (equal '(1 2) (hh-mm-diff
			 '(1 59) '(3 01))))
  (should-error (hh-mm-diff '(3 00) '(2 25))
		:type 'times-order-error)
  (should-error (hh-mm-diff '(3 26) '(3 25))
		:type 'times-order-error))

;; hh-mm-add adds two clock times (each given in hour and minute)

(defun hh-mm-add (time1 time2)
  "Add two times TIME1 and TIME2 each given as list of hours and minutes.

Return sum of the 2 times as list with hours and minutes.
Watch out: hours sum is allowed to exceed 23 or 24!"
  (let* ((h1 (car time1))
         (m1 (cadr time1))
         (h2 (car time2))
         (m2 (cadr time2))
         (m-sum (+ m1 m2)))
    (if (>= m-sum 60)
        (list (+ h1 h2 1) (- m-sum 60))
      (list (+ h1 h2) m-sum))))

(ert-deftest me-org-hh-mm-add ()
  "Test hh-mm-add with common cases"
  (should (equal '(19 0) (hh-mm-add
			 '(17 30) '(1 30))))
  (should (equal '(0 59) (hh-mm-add
			 '(0 30) '(0 29))))
  (should (equal '(25 0) (hh-mm-add
			 '(12 30) '(12 30))))
  (should (equal '(18 1) (hh-mm-add
			 '(17 59) '(0 2))))
  (should (equal '(13 59) (hh-mm-add
			 '(10 29) '(3 30))))
  (should (equal '(21 58) (hh-mm-add
			 '(10 59) '(10 59)))))

;;; org-mode specific helper functions

;; narrow an existing org buffer to a day entry

(defun me-org-narrow-to-day (&optional tree-level)
  "Narrow to current level 3 org-journal-subtree"
  (interactive)
  (let ((tree-heading-level (or tree-level 3))
        (org-elm (org-element-at-point)))
    (if (not (equal 'headline (org-element-type org-elm)))
        (outline-previous-heading))
    (while (> (car (org-heading-components)) tree-heading-level)
      (outline-up-heading 1))
    (org-narrow-to-subtree)))

(defun me-org-tags-n-durations-in-current-buffer ()
  "Narrow to superior/current level 3 headlines with all content
esp. level 4 headlines. Parse org element and evaluate contained
tags and associated durations and display the results."
  (interactive)
  (me-org-narrow-to-day)
  (condition-case err ;; try-block
      (let* ((org-tree (org-element-parse-buffer))
             (times-n-tags (me-org-times-n-tags-of-subheadlines org-tree))
             (tags-n-durations
              (me-org-times-n-tags-to-tags-n-durations times-n-tags)))
        (goto-char (point-max))
        (newline)
        (insert (me-org-tags-n-durations-to-string tags-n-durations))
        (widen))
    ;; catch me-times-order-error
    (me-times-order-error
     (message "%s %s" (error-message-string err) (cdr err))
     (widen)))) ;; widen buffer in specific error case too (again)

(global-set-key (kbd "<f8>") 'me-org-tags-n-durations-in-current-buffer)

;; prepare *org-scratch* with demo content

(defvar me-org-tree
  nil
  "Holds org tree from parsing an org
structure with parser of org-element.el")

(defun me-org-setup-org-scratch ()
  "Makes or emptys buffer *org-scratch* and
sets org-mode and inserts a org structure"
  (interactive)
  (if (not (get-buffer "*org-scratch*"))
    (get-buffer-create "*org-scratch*"))
  (with-current-buffer "*org-scratch*"
    (erase-buffer)
    (org-mode)
    (insert #("* [2023-08-14 Mon 09:08] - Test :foo:bar:
"))
    (setq me-org-tree
	  (org-element-parse-buffer))))
    
(defun me-org-setup-org-scratch-complex ()
  "Fill *org-mode* buffer with demo content"
  (interactive)
  (if (not (get-buffer "*org-scratch*"))
    (get-buffer-create "*org-scratch*"))
  (when (set-buffer "*org-scratch*")
    (erase-buffer)
    (org-mode)
    (insert (format
             "-*- mode: Org; -*-
#+STARTUP: hidestars content
#+TITLE: org file demo content for parsing
* a
roeuch
** [2023-08-08 Di 09:00] - aa                                           :bar:
** [2023-08-08 Di 12:45] - ab                                           :foo:
Trallala
und Hoppsassa
** [2023-08-08 Di 14:00] - Pauze
** [2023-08-08 Di 14:45] - ac                                           :foo:
** [2023-08-08 Di 15:30] - ad                                           :bar:
** [2023-08-08 Di 16:30] - ae                                       :foo:bar:
Ah wa'!
** [2023-08-08 Di 17:00] - Schluzz jetzz - 5:00 bar, 2:15 foo (7:15)
Noch mehr Text?
Ja eine Zeile!
"))))

(defun me-org-parse-org-scratch (&optional granularity) 
  "Parse demo org tree in buffer *org-scratch* to me-org-tree
s. org-element-parse-buffer for explanation of GRANULARITY

Returns parsed org structure - and setting variable ME-ORG-TREE to

Common usage: (me-org-parse-org-scratch 'headline)"
  (interactive) 
  (set-buffer "*org-scratch*")
  (setq me-org-tree
	(org-element-parse-buffer (or granularity 'object))))

(defun me-org-parsetree ()
  "Should be called in a org-mode buffer, best in an narrowed
indirected buffer (for efficiency). Resulting tree is assigned to me-org-tree."
  (interactive)
  (setq me-org-tree ; side effect: bind the parsetree to a variable
        (org-element-parse-buffer)))

(defun me-org-parsetree-headlines ()
  "Should be called in a org-mode buffer, best in an narrowed
indirected buffer (for efficiency). Resulting tree is assigned to me-org-tree."
  (interactive)
  (setq me-org-tree ; side effect: bind the parsetree to a variable
        (org-element-parse-buffer 'headline)))

;;; transforming a (time ordered list) of times with or without tag(s)
;;; to a assoc list: each tag and each combination of tags point are
;;; associated with a summed up time for a tagged timestamp

(defun me-org-times-n-tags-to-tags-n-durations (times-n-tags)
  "setting up (first) reference time and going into recursion"
  (let ((ref-time (caar times-n-tags))
        (tags (cadar times-n-tags))
        (tags-n-durations ()))
    (if tags
        (user-error "Unexpected tags %s for last time in list!" tags)
      (me-org-times-n-tags-to-tags-n-durations-rec
       (cdr times-n-tags)
       tags-n-durations
       ref-time
       '(0 0)))))

(defun me-org-times-n-tags-to-tags-n-durations-rec
    (times-n-tags tags-n-durations ref-time summarized-duration)
  "TIMES-N-TAGS is a list of entries of a day but reversed ordered
(first element is finalizing entry of day, last is first entry of day.
Process first list element of START-TIMES-N-TAGS-LIST and do following:
Lookup current head's tags of TIMES-N-TAGS in TAGS-N-DURATION (- if
there is already an associated pair in previous collected TAGS-N-DURATIONS).
IF, update associaciated pair in TAGS-N-DURATIONS by adding the difference
of current head's time of TIMES-N-TAGS to REF-TIME to the already recorded
duration in associated pair in TAGS-N-DURATIONS.
If not, push current current head's tags with time differenc of current
head's time of TIMES-N-TAGS to REF-TIME.
(Only) if PAUSED flag is nil add calculated current duration to
SUMMARIZED-DURATION.
Recursion step with rest of TIMES-N-TAGS, updated TAGS-N-DURATIONS, current
head's time as (new) ref-time, updated SUMMARIZED-DURATION and updatet PAUSED
flag.
Special case: recursion end because TIMES-N-TAGS ran empty - finalize
TAGS-N-DURATIONS by pushing a last record (a pair of list with tag SUMMARIZED
and SUMMARIZED-DURATION."
  (if times-n-tags
      (let* (;; extract time and tags from current head of times-n-tags
             (current-time (caar times-n-tags))
             (tags (cadar times-n-tags))
             ;; calculate time difference to (previous) ref-time
             (current-duration (hh-mm-diff current-time ref-time))
             ;; update summarized-duration only when paused flag is nil
             (updated-summarized-duration
              (if (and tags t)
                  (hh-mm-add summarized-duration current-duration)
                summarized-duration))
             ;; try to extract previous tags-n-duration based on tags
             (previous-tags-n-duration (assoc tags tags-n-durations))
             ;; calculate time difference from current time to ref-time
             ;; and build a new pair of tags and new or updated duration
             (new-tags-n-duration
              (list tags
                    (if previous-tags-n-duration
                        (hh-mm-add
                         current-duration
                         (cadr previous-tags-n-duration))
                      current-duration)))
             ;; update tags-n-durations by consing new-tags-n-duration
             ;; as head to tags-n-durations (which's
             ;; previous-tags-n-duration is removed from tags-n-durations
             (updated-tags-n-durations
              (cons new-tags-n-duration
                    (if previous-tags-n-duration
                        (assoc-delete-all tags tags-n-durations)
                      tags-n-durations))))
        ;; and finally handle rest of start-times-with-tags-list recursive
        ;; with all the new build material! paused flag is t, if tags exist
        (me-org-times-n-tags-to-tags-n-durations-rec
         (cdr times-n-tags)
         updated-tags-n-durations
         current-time
         updated-summarized-duration))
    ;; else (means start-times-with-tags-list ran empty) return
    ;; tags-n-durations with summarized time pushed
    (cons (list (list "SUMMARIZED") summarized-duration)
          tags-n-durations)))

(defun me-org-test-tnt2tnd ()
  "performs the me-org-times-n-tags-to-tags-n-durations function with
a real word test-list"
  (me-org-times-n-tags-to-tags-n-durations
   '(((17 0) ())
     ((16 30) ("foo" "bar"))
     ((15 30) ("bar"))
     ((14 45) ("foo"))
     ((14 0) ())
     ((12 45) ("foo"))
     ((9 0) ("bar")))))

;; expected result: (((bar) (4 45)) ((foo) 2 0)) (nil (0 45)) ((foo bar) (0 30))
;; or total 7 15  break 0 45
;; can manually interpreted to 5:00 bar, 2:15 foo, total 7:15

;;; functions to display tags and durations

(defun me-org-tags-n-durations-to-string (tags-n-durations-list)
  "returns string with tags and durations a bit beautified"
  (let ((tags-n-durations-rest (copy-alist tags-n-durations-list))
	(accu-string ""))
    (cl-loop for tags-string-list in me-org-tags-order-abbrevs do
	     (let ((duration (cadr (assoc (car tags-string-list)
				   tags-n-durations-list))))
	       (when duration
		 (let* ((tagstring (cadr tags-string-list))
			(durationstring (format " %d:%02d   "
						(car duration)
						(cadr duration))))
		   (setq accu-string
			 (concat accu-string tagstring durationstring))
		   (setq tags-n-durations-rest
			 (remove
			  (list (car tags-string-list) duration)
			  tags-n-durations-rest))))))
    (cl-loop for tags-n-duration in (copy-alist tags-n-durations-rest) do
	     (let* ((tagstring (if (car tags-n-duration)
				   (string-join
				    (mapcar
				     (lambda (elm)
				       (format "%s" elm))
				     (car tags-n-duration))
				    ":")
				 "Pause"))
		    (duration (cadr tags-n-duration))
		    (durationstring (format " %d:%02d "
					    (car duration)
					    (cadr duration))))
	       (setq accu-string
		     (concat accu-string " +" tagstring durationstring))
	       (setq tags-n-durations-rest
		     (remove tags-n-duration tags-n-durations-rest))))
    (if tags-n-durations-rest
	(error "UNEXPECTED rest list tags-n-durations %s"
	       tags-n-durations-rest)
      accu-string)))

;;; analysing org-tree
;;;  - function to extract in inactive timestamp and tags (if given)
;;;    from a org-tree node (expected: a headline)
;;;    purpose: display time (hour minute) and tag(s)
;;;  - function to extract properties

(defun me-org-time-and-tags (node accu-times-n-tags)
  "NODE (headline node) is expected to have
a title propertry and this title again contains an inactive timestam
with its properties hour-start and minute-start.

If there is a timestamp return a list consisting of a new
LIST of time (list) and tags (list) which will be consed with
ACCU-TIME-N-TAGS.

Is not timestamp found, return accu-times-n-tags unaltered."
  (let* ((plist
	  (if (plist-with-symbols-as-keys-p (cadr node))
	      (cadr node)))
	 (title-plist (plist-get plist ':title))
	 (tags (plist-get plist ':tags))
	 (timestamp (if title-plist
			(plist-get (car title-plist) 'timestamp))))
    (if (and plist timestamp)
	;; extract time and tags (if given) and push em to
	;; ACCU-TIMES-N-TAGS
	(let ((hour (plist-get timestamp ':hour-start))
	      (minute (plist-get timestamp ':minute-start))
              (cleaned-tags (mapcar
                             (lambda (tag)
                               (me-remove-text-properties tag))
                             tags)))
          (message
	   (prettystring
	    (format "TIME %d %d : TAGGED %s" hour minute cleaned-tags)
	    78))
          (setq accu-times-n-tags
	        (cons (list (list hour minute) cleaned-tags)
		      accu-times-n-tags)))
      ;; (elso) no timestamp - return ACCU-TIMES-N-TAGS unmodified
      accu-times-n-tags)))

(defun me-org-times-n-tags-of-subheadlines (node &optional accu-times-n-tags)
  "Traverses a org structure beginning with given NODE.
For this, first headline is detected - and all direct sub headlines
(headlines on level higher
 - first find a first headline node which is taken as level 1 headline
 - then find all direct child headline nodes as level 2 headlines
   and accumulate their times and tags combinations into ACCU-TIMES-N-TAGS
Accumulation is done by prepending each found combination at the beginning
of result list.
Returns accumulated list, which time order is reversed (last journaling
time first to first journaling time."
  (let ((tree-to-exam node)
	(new-accu-times-n-tags accu-times-n-tags))
    (when (equal 'org-data (car tree-to-exam))
      ;;; org-data node?
      ;;;   drop following list elements which are not 'headline (lists)
      (message "Root node ORG-DATA found - drop its and its following nil")
      ;; examine nodes behind org-data (symbol) and associated nil
      (setq tree-to-exam (cddr tree-to-exam))
      ;; iterate those nodes until first headline as SUPERIOR headline found
      (while (and
              tree-to-exam
              (not (equal 'headline (caar tree-to-exam))))
	(setq tree-to-exam (cdr tree-to-exam)))
      ;; further processing (only) if there is a superior headline
      (if tree-to-exam
          (progn
            (message "FOUND superior headline - now analyse level 2 headlines")
            ;; examine ONLY org elements DIRECTLY BELOW found superior headline
            (setq tree-to-exam (car tree-to-exam))
            ;; drop superior 'headline (symbol) and its (associated) plist
            (setq tree-to-exam (cddr tree-to-exam))
            ;; iterate all DIRECT child nodes & process headline child nodes
            (mapc
             (lambda (direct-child-node)
               (if (equal 'headline (car direct-child-node))
                   (setq new-accu-times-n-tags
	                 (me-org-time-and-tags
                          direct-child-node
                          new-accu-times-n-tags))))
             tree-to-exam))
        (message "NO superior headline found!")))
    ;; whatever collected before - return new-accu-times-n-tags
    new-accu-times-n-tags))

(defun me-org-test-extract ()
  "Build *org-scratch* buffer; populate; parse - and extract tags-n-times"
  (interactive)
  (me-org-setup-org-scratch-complex)
  (me-org-parse-org-scratch)
  (let* ((times-n-tags (me-org-times-n-tags-of-subheadlines
                        me-org-tree))
         (tags-n-durations (me-org-times-n-tags-to-tags-n-durations
                            times-n-tags)))
    (me-org-display-tags-n-durations tags-n-durations)))

;;;
;;;
;;; 2023-08-26 following 2 functions are rotten now
;;;
;;;

(defun me-org-node-details (elem org-symbol criterium-pair accu-times-n-tags
				 &optional tree-level)
  "Handle single ELEM of an org list, either an org type
or an org object or a string or something else.
ORG-SYMBOL is the org symbol (object/element) to search for.
ACCU-TIMES-N-TAGS is a list holding all matching times with tags.
TREE-LEVEL (default 0) specifies indentation and should reflect
recursion level in diagnostic messages.
Return modified or unmodified list of times-n-tags."
  (let ((level-pad (make-string (* (or tree-level 0) 2) ?\ )))
    (cond
     ((stringp elem)
      ;; ELEM is just a raw string
      (message (prettystring
		(format "%sRAWSTRING %s"
			level-pad
			(me-remove-text-properties elem))
		78))
      accu-times-n-tags)
     ((plist-with-symbols-as-keys-p elem)
      ;; a plist? check CRITERIUM-PAIR
      (let ((val (plist-get elem (car criterium-pair))))
        (when (equal val (cadr criterium-pair))
	  ;; CRITERIUM-PAIR matches
          (message "%sFOUND::: %s" level-pad criterium-pair)
          (me-org-time-and-tags elem accu-times-n-tags))))
     ((listp elem)
      ;; any other list? handle recursive
      (message "%sRECURSE NEXT TREE-LEVEL" level-pad)
      (me-org-find elem org-symbol criterium-pair accu-times-n-tags
		   (1+ (or tree-level 0))))
     (t
      ;; Oopsy - this is unexpected in an proper (parsed) org structure
      (message (prettystring (format "%sUNEXPECTED %s" level-pad elem) 78))
      accu-times-n-tags))))

(defun me-org-find (node org-symbol criterium-pair accu-times-n-tags
			 &optional tree-level)
  "Traverses NODE (an org root or a subnode in an org-element
parse tree).

Return each specified ORG-SYMBOL with matching CRITERIUM-PAIR, a
two elements list, that specifies the org-property and its
value, for example \'(:level 2)
TREE-LEVEL specifies start intendation of diagnostic messages"
  (interactive)
  (let ((level-pad (make-string (* (or tree-level 0) 2) ?\ )))
    (cond
     ((stringp node)
      ;; TODO Clarify, if breaking recursion here is okay
      (message (prettystring
                (format "%sRAWSTRING %s"
                        level-pad
                        (me-remove-text-properties node)) 80))
      accu-times-n-tags)
     ((equal (car node) 'org-data)
      ;; list head (car) is an org root node; ignore following element
      ;; "nil"; iterate following rest list (cddr) of org
      ;; (sub)objects/(sub)elements
      (message "ROOT NODE org-data")
      (mapc
       (lambda (elem)
         (me-org-find elem org-symbol criterium-pair accu-times-n-tags
		      tree-level))
       (cddr node))
      accu-times-n-tags)
     ((member (car node) org-element-all-objects)
      ;; list head (car) is an org OBJECT; iterate following rest list
      ;; (cdr) of org (sub)objects/(sub)elements
      (message (prettystring
                (format "%sOBJ %s (Len: %s) -> %s"
                        level-pad (car node) (length node) (cdr node))
                78))
      (mapc
       (lambda (elem)
         (me-org-node-details elem org-symbol criterium-pair accu-times-n-tags
			      tree-level))
;;         (me-org-find elem org-symbol criterium-pair accu-times-n-tags
;;		      tree-level))
       (cdr node))
      accu-times-n-tags)
     ((member (car node) org-element-all-elements)
      ;; list head (car) is an org ELEMENT; iterate following rest list
      ;; (cdr) which is a plist (properties) and 0 to n org-elements (nodes)
      (message (prettystring
                (format "%sTYPE %s -> %s"
                        level-pad (car node) (cdr node))
                78))
      (mapcar
       (lambda (elem)
         (me-org-node-details elem org-symbol criterium-pair accu-times-n-tags
			      tree-level))
       (cdr node))
      accu-times-n-tags)
     (t
      ;; Oopsy - this is unexpected in an proper (parsed) org structure
      (message (prettystring
		(format "UNKNOWN %s"
			node) 78))
      accu-times-n-tags))))

;;; 2023-08-26 END following 2 functions are rotten now


;;           (me-org-find me-org-tree 'headline '(:level 4) 0)
;;
;; could/should traverse a real word journal tree with this structure
;; * Year
;; ** Month
;; *** Day
;; **** journal entry with an inactive timestamp as start time
;; **** journal entry
;; **** journal entry
;;
;; at [2023-08-23 Wed] too buggy:
;;  - recurses (much) too deep - should stop at CRITERIUM-PAIR match
;;  - built result list is much too garbled
;;
;; Uhnääh! I have to reassess the three involved functions!!1!
;;


;;; me-org-addons.el ends here
