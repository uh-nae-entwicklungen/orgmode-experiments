# orgmode experiments

Elisp code snippets and additions to enhance famous Emacs' org-mode.

## Why?

As an Emacs enthusiast noticed in an YouTube interview (https://www.youtube.com/watch?v=urcL86UpqZc: at 00:01:17): "Two words: org mode". I think there is nothing to add. :)

## License

GPL v3

## Project status

It's more a collection of ideas, small tools than a project.

As of 2023, August - this here is at the very beginning - some of my snippets still existed.
